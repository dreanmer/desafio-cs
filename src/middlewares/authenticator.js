var jwtHelper = require('../helpers/jwt-helper.js');

module.exports = function (req, res, next) {
    var token = req.headers.authorization;

    if(!token)
        return res.sendError('não autenticado', 401);

    token = token.replace('Bearer ', '');
    jwtHelper.verifyToken(token, function(err, uid){
        if(err) return res.sendError('token inválido', 401);

        req.uid = uid;
        next();
    });
};