module.exports = function (req, res, next) {
    res.sendError = function (message, status) {
        if(status && typeof status === "number")
            this.status(status);

        return this.json({
            success: false,
            message: message
        });
    };

    res.sendSuccess = function (message) {
        return this.json({
            success: true,
            message: message
        });
    };

    next();
};