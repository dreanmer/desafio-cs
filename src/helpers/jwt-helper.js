var jwt = require('jsonwebtoken');

module.exports = {

    genToken: function (user, expiration) {
        token = jwt.sign({uid: user._id}, process.env.SECRET, {
            expiresIn: (expiration || '30m')
        });

        user.token = token;
        user.save();

        return token;
    },

    verifyToken: function (token, cb) {
        jwt.verify(token, process.env.SECRET, function(err, decoded) {
            if (err) cb(err);
            else cb(null, decoded.uid);
        });
    }
};
