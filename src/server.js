var express = require('express'),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    authenticator = require('./middlewares/authenticator.js'),
    app = express();

mongoose.connect('mongodb://localhost/', function(err) {
    if (err) throw err;
    console.log('Successfully connected to MongoDB');
});

// middlewares
app.use(bodyParser.json());
app.use(require('./middlewares/messager.js'));

// routes
app.get('/', function(req, res){
    res.sendSuccess('Api online');
});

app.use('/authenticate', require('./routes/authenticate.js'));
app.use('/register', require('./routes/register.js'));
app.use('/user', authenticator, require('./routes/user.js'));

// not found
app.use(function (req, res) {
    res.sendError("404 - não encontrado", 404);
});

// start server
var server = app.listen(process.env.SERVER_PORT || 8080, function () {
    console.log('Server runing on port: %s', server.address().port);
});
