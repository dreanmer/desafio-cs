var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    bcrypt = require('bcrypt-nodejs'),
    SALT_WORK_FACTOR = 10;

var PhoneSchema = new Schema({
    "numero": {type: String, match: /^[\d -]{8,11}$/},
    "ddd": {type: String, match: /^\d{2}$/}
});

var UserSchema = new Schema({
    "nome": {type: String},
    "telefones": [PhoneSchema],
    "email": {type: String, required: true, index: {unique: true}},
    "senha": {type: String, required: true},
    "data_criacao": {type: Date},
    "data_atualizacao": {type: Date},
    "ultimo_login": {type: Date},
    "token": {type: String}
});

UserSchema.pre('save', function (next) {
    var user = this;

    if (!user.isModified('senha')) return next();

    this.data_criacao = this.ultimo_login = new Date();

    bcrypt.hash(user.senha, null, null, function (err, hash) {
        if (err) return next(err);

        user.senha = hash;
        next();
    });
});

UserSchema.methods.comparePassword = function (pass, cb) {
    bcrypt.compare(pass, this.senha, function (err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

var User = mongoose.model('User', UserSchema);

module.exports = User;
