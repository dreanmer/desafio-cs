var router = require('express').Router(),
    User = require('../models/user.js');

router.get('/:id', function(req, res){
    if(req.params.id != req.uid)
        return res.sendError('não autorizado', 403);

    User.findById(req.params.id, function(err, user) {
        if(err) return res.sendError(err.toString());
        if(!user) return res.sendError('invalid token and id', 500);

        return res.json({
            success: true,
            data: user
        });
    });
});

router.get('/delete/:id', function(req, res){
    if(req.params.id != req.uid)
        return res.sendError('não autorizado', 403);

    User.findById(req.params.id, function(err, user) {
        if(err) return res.sendError(err.toString(), 500);
        if(!user) return res.sendError('invalid token and id', 500);
        user.remove();
        return res.sendSuccess('usuário deletado');
    });
});

module.exports = router;
