var router = require('express').Router(),
    jwtHelper = require('../helpers/jwt-helper.js'),
    User = require('../models/user.js');

router.post('/', function (req, res) {
    User.findOne({'email': req.body.email}, function (err, user) {
        if(!user)
            return res.sendError('email e/ou senha inválidos');

        user.comparePassword(req.body.senha, function(err, isMatch){
            if(!isMatch)
                return res.sendError('email e/ou senha inválidos', 401);

            var token = jwtHelper.genToken(user);

            user.ultimo_login = new Date();
            user.save();

            user = user.toObject();
            user.id = user._id;
            delete user.__v;
            delete user.token;
            delete user.senha;
            delete user._id;

            return res.send({
                success: true,
                data: user,
                token: token
            });
        });
    });
});

module.exports = router;
