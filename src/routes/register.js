var router = require('express').Router(),
    jwtHelper = require('../helpers/jwt-helper.js'),
    User = require('../models/user.js');

router.post('/', function (req, res) {
    var newUser = new User(req.body);

    User.findOne({email: req.body.email}, function (err, user) {
        if (err) return res.sendError(err.toString());
        if (user) return res.sendError('email já existente', 200);

        newUser.save(function (err) {
            if (err) return res.sendError(err.toString());

            var token = jwtHelper.genToken(newUser);

            user = newUser.toObject();
            user.id = user._id;
            delete user.__v;
            delete user.token;
            delete user.senha;
            delete user._id;

            return res.json({
                success: true,
                data: user,
                token: token
            });
        });
    });

});

module.exports = router;
