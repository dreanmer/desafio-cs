# Desafio node.js Concrete Solutions

# todo

- aplicar jslint/hint  

# instruções de instalação:

- dependencias: git, node, npm, gulp, mongodb
- npm install
- gulp start

# para testar a api
depende da api estar online, o teste é feito sobre os metodos da api

- gulp test

# api:

api segue um padrão de response:

para mensagens:

    {
        "success": false,
        "message": "email já existente"
    }

e para dados:

    {
        "success": true,
        "data": {
            "dados": "requisitados"
        }
        "token": "JWT_TOKEN_STRING"
    }

## POST - /register
use esta rota para registrar um novo usuário (e obter um token de acesso), passando no corpo da request o seguinte formato de json:
    
    {
      "nome": "nome do usuário",
      "email": "email@dominio.com",
      "senha": "senha",
      "telefones": [
        {
          "numero": "123456789",
          "ddd": "11"
        }
      ]
    }
    
response:

    {
        "success": true,
        "data": {
            "data_criacao": "2016-05-30T13:11:05.323Z",
            "ultimo_login": "2016-05-30T13:11:05.323Z",
            "nome": "nome do usuário",
            "email": "email@dominio.com",
            "id": "574c3be9d149efe1010dc025",
            "telefones":[
            {
                "numero": "123456789",
                "ddd": "11",
                "_id": "574c3be9d149efe1010dc026"
            }
        ]
    },
        "token": "JWT_TOKEN_STRING"
    }

## POST - /authenticate
use esta para obter um token de acesso para um usuário já existente, passando no corpo da request o seguinte formato de json:

    {
      "email": "email@dominio.com",
      "senha": "senha"
    }

response

    {
        "success": true,
        "data": {
            "data_criacao": "2016-05-30T13:11:05.323Z",
            "ultimo_login": "2016-05-30T13:11:05.323Z",
            "nome": "nome do usuário",
            "email": "email@dominio.com",
            "id": "574c3be9d149efe1010dc025",
            "telefones":[
            {
                "numero": "123456789",
                "ddd": "11",
                "_id": "574c3be9d149efe1010dc026"
            }
        ]
    },
        "token": "JWT_TOKEN_STRING"
    }

## GET - /user/:id
o usuário pode obter os detalhes persistidos no servidor utilizando-se do id (obtido ao se registrar ou autenticar), também precisa de um "Authorization token" no cabeçalho da request no formato: ("Bearer {token}");

response: 

    {
        "success": true,
        "data": {
            "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiI1NzRjM2Q2NDAyZDQzZjQyMDY4OWUzZTAiLCJpYXQiOjE0NjQ2MTQyNDQsImV4cCI6MTQ2NDYxNjA0NH0.2ik-vQhY6o0ALgcSyy1svrSeyDQx2kMbDs4fIQzEakw",
            "__v": 0,
            "data_criacao": "2016-05-30T13:17:24.102Z",
            "ultimo_login": "2016-05-30T13:17:24.102Z",
            "nome": "nome do usuário",
            "email": "emails@dominio.com",
            "senha": "$2a$10$o8oZuH5SJ2CGMHLKSmyovOIa3feSCxdfV1w.j9FpljPBCVf8lmBR2",
            "_id": "574c3d6402d43f420689e3e0",
            "telefones": [
                {
                    "numero": "123456789",
                    "ddd": "11",
                    "_id": "574c3d6402d43f420689e3e1"
                }
            ]
        }
    }
    

# Desafio node.js Concrete Solutions

Crie um aplicativo backend que exporá uma API RESTful de criação de sing up/sign in.

Todos os endpoints devem somente aceitar e somente enviar JSONs. O servidor deverá retornar JSON para os casos de endpoint não encontrado também.

O aplicativo deverá persistir os dados (ver detalhes em requisitos).

Todas as respostas de erro devem retornar o objeto:

{ "mensagem": "mensagem de erro" }

Segue a documentação dos endpoints:

### Criação de cadastro

- ok - Este endpoint deverá receber um usuário com os seguintes campos: nome, email, senha e uma lista de objetos telefone. Seguem os modelos:

  { "nome": "string",
    "email": "string",
    "senha": "senha",
    "telefones": [
       {
         "numero": "123456789",
         "ddd": "11"
       }
    ]
  }
  
- ok - Usar status codes de acordo
- ok - Em caso de sucesso irá retornar um usuário mais os campos:
  - `id`: id do usuário (pode ser o próprio gerado pelo banco, porém seria interessante se fosse um GUID)
  - `data_criacao`: data da criação do usuário
  - `data_atualizacao`: data da última atualização do usuário
  - `ultimo_login`: data do último login (no caso da criação, será a mesma que a criação)
  - `token`: token de acesso da API (pode ser um GUID ou um JWT)
- ok - Caso o e-mail já exista, deverá retornar erro com a mensagem "E-mail já existente".
- ok - O token deverá ser persistido junto com o usuário

### Sign in

- ok - Este endpoint irá receber um objeto com e-mail e senha.
- ok Caso o e-mail exista e a senha seja a mesma que a senha persistida, retornar igual ao endpoint de sign_up.
- ok - Caso o e-mail não exista, retornar erro com status apropriado mais a mensagem "Usuário e/ou senha inválidos"
- ok - Caso o e-mail exista mas a senha não bata, retornar o status apropriado 401 mais a mensagem "Usuário e/ou senha inválidos"

### Buscar usuário

- ok - Chamadas para este endpoint devem conter um header na requisição de Authentication com o valor "Bearer {token}" onde {token} é o valor do token passado na criação ou sign in de um usuário.
- ok - Caso o token não exista, retornar erro com status apropriado com a mensagem "Não autorizado".
- ok - Caso o token exista, buscar o usuário pelo user_id passado no path e comparar se o token no modelo é igual ao token passado no header.
- ok - Caso não seja o mesmo token, retornar erro com status apropriado e mensagem "Não autorizado"
- ok - Caso seja o mesmo token, verificar se o último login foi a MENOS que 30 minutos atrás.
- ok - Caso não seja a MENOS que 30 minutos atrás, retornar erro com status apropriado com mensagem "Sessão inválida".
- ok - Caso tudo esteja ok, retornar o usuário.

## Requisitos

- ok - persitência de dados
- Sistema de build
    - ok - Gestão de dependências via gerenciador de pacotes
    Utilizar um task runner para realização de build
- Padronização de estilo de código em tempo de build - sugestão: jsHint/jsLint
- ok - API: Express

## Requisitos desejáveis

- ok - JWT como token
- ok - Testes unitários
- ok - Criptogafia não reversível (hash) na senha e no token
