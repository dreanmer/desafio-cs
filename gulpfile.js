var gulp = require('gulp'),
    nodemon = require('nodemon'),
    mocha = require('gulp-mocha'),
    jshint = require('gulp-jshint');

gulp.task('lint', function () {
    return gulp.src('./lib/*.js')
        .pipe(jshint({linter: require('jshint-jsx').JSXHINT}))
});

gulp.task('test', function () {
    return gulp.src('test/tests.js', {read: false})
        .pipe(mocha({reporter: 'nyan'}));
});

gulp.task('start', function () {
    nodemon({
        script: 'src/server.js',
        env: {
            'NODE_ENV': 'development',
            'SERVER_PORT': 8080,
            'SECRET': 'myJWTsecretIS$$superSecret'
        }
    })
});