var supertest = require("supertest"),
    server = supertest.agent("http://localhost:8080");

require("should");

function login(callback) {
    server.post('/authenticate')
        .send({
            "email": "tester@gmail.com",
            "senha": "password"
        })
        .expect("Content-type", /json/)
        .expect(200)
        .end(function (err, res) {
            res.body.should.have.property('success').with.equal(true);
            var token = res.body.token,
                user = res.body.data;

            callback(token, user);
        });
}

describe("Server api test", function () {

    it("should return api status", function (done) {
        server.get('/')
            .expect("Content-type", /json/)
            .expect(200)
            .end(function (err, res) {
                res.body.should.have.property('success').with.equal(true);
                res.body.should.have.property('message').of.type('string');
                done();
            });
    });

    it("should register an user", function (done) {
        server.post('/register')
            .send({
                "nome": "alexandre reis",
                "email": "tester@gmail.com",
                "senha": "password",
                "telefones": [
                    {
                        "numero": "97101-8746",
                        "ddd": "11"
                    }
                ]
            })
            .expect("Content-type", /json/)
            .expect(200)
            .end(function (err, res) {
                res.body.should.have.property('success').with.equal(true);
                done();
            });
    });

    it("should not register an duplicated user", function (done) {
        server.post('/register')
            .send({
                "nome": "alexandre reis",
                "email": "tester@gmail.com",
                "senha": "password",
                "telefones": [
                    {
                        "numero": "97101-8746",
                        "ddd": "11"
                    }
                ]
            })
            .expect("Content-type", /json/)
            .expect(200)
            .end(function (err, res) {
                res.body.should.have.property('success').with.equal(false);
                user = res.body.data;
                done();
            });
    });

    it("should fail on auth with an invalid password", function (done) {
        server.post('/auth')
            .send({
                "email": "tester@gmail.com",
                "senha": "invalidpass"
            })
            .expect("Content-type", /json/)
            .expect(401)
            .end(function (err, res) {
                res.body.should.have.property('success').with.equal(false);
                done();
            });
    });

    it("should not retrieve user info without jwt", function (done) {
        server.get('/user/1231231231')
            .expect("Content-type", /json/)
            .expect(401)
            .end(function (err, res) {
                res.body.should.have.property('success').with.equal(false);
                done();
            });
    });

    it("should fail on auth with an invalid email", function (done) {
        server.post('/auth')
            .send({
                "email": "invalid@email.com",
                "senha": "password"
            })
            .expect("Content-type", /json/)
            .expect(401)
            .end(function (err, res) {
                res.body.should.have.property('success').with.equal(false);
                done();
            });
    });

    it("should auth with success and retrieve user info with correct jwt", function (done) {
        login(function (token, user) {
            server.get('/user/' + user.id)
                .set('Authorization', 'Bearer ' + token)
                .expect("Content-type", /json/)
                .expect(200)
                .end(function (err, res) {
                    res.body.should.have.property('success').with.equal(true);
                    done();
                });
        })
    });

    it("should auth with success and fail to retrieve other user info with correct jwt", function (done) {
        login(function (token, user) {
            server.get('/user/' + user.id + '1')
                .set('Authorization', 'Bearer ' + token)
                .expect("Content-type", /json/)
                .expect(200)
                .end(function (err, res) {
                    res.body.should.have.property('success').with.equal(false);
                    done();
                });
        });
    });

    it("should auth with success and delete test user with correct jwt", function (done) {
        login(function (token, user) {
            server.get('/user/delete/' + user.id)
                .set('Authorization', 'Bearer ' + token)
                .expect("Content-type", /json/)
                .expect(200)
                .end(function (err, res) {
                    res.body.should.have.property('success').with.equal(true);
                    done();
                });
        });
    });

    it("should return 404 to a non existant route", function (done) {
        server.get('/non/existent/route')
            .expect("Content-type", /json/)
            .expect(404)
            .end(function (err, res) {
                res.status.should.equal(404);
                res.body.success.should.equal(false);
                res.body.should.have.property('message').of.type('string');
                done();
            });
    })
});